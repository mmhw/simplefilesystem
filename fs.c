#include "fs.h"
#include "disk.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#define FS_MAGIC           0xf0f03410
#define INODES_PER_BLOCK   128
#define POINTERS_PER_INODE 5
#define POINTERS_PER_BLOCK 1024


char free_data_blocks_bitmap[1000];

struct fs_superblock
{
	int magic;
	int nblocks;
	int ninodeblocks;
	int ninodes;
};

struct fs_inode
{
	int isvalid;
	int size;
	int direct[POINTERS_PER_INODE];
	int indirect;
};

union fs_block
{
	struct fs_superblock super;
	struct fs_inode inode[INODES_PER_BLOCK];
	int pointers[POINTERS_PER_BLOCK];
	char data[DISK_BLOCK_SIZE];
};

int fs_format()
{
    union fs_block block;

    int ninodeblocks = get_n_inode_blocks();

    block.super.magic = FS_MAGIC;
    block.super.nblocks = disk_size();
    block.super.ninodeblocks = ninodeblocks;
    block.super.ninodes = block.super.ninodeblocks*INODES_PER_BLOCK;

    disk_write(0, block.data);

    for (size_t i = 0; i < INODES_PER_BLOCK; i++)
    {
        block.inode[i].isvalid = 0;
    }

    for (size_t i = 1; i <= ninodeblocks; i++)
    {
        disk_write(i, block.data);
    }

	return 1;
}

int fs_mount()
{
    int ninodeblocks = get_n_inode_blocks();
	union fs_block block;

    for (size_t i = 1; i <= ninodeblocks; i++)
    {
        disk_read(i,block.data);
        for (size_t j = 0; j < INODES_PER_BLOCK; j++)
        {
            if (block.inode[j].isvalid == 1)
            {
                if (block.inode[j].size > 0)
                {
                    for (size_t k = 0; k < POINTERS_PER_INODE; k++)
                    {
                        if (block.inode[j].direct[k] == 0) break;
                        free_data_blocks_bitmap[block.inode[j].direct[k]] = 1;
                    }
                    if (block.inode[j].indirect != 0)
                    {
                        free_data_blocks_bitmap[block.inode[j].indirect] = 1;

                        union fs_block pointers_block;

                        disk_read(block.inode[j].indirect, pointers_block.data);

                        for (size_t l = 0; l < POINTERS_PER_BLOCK; l++)
                        {
                            if (pointers_block.pointers[l] == 0) break;
                            free_data_blocks_bitmap[pointers_block.pointers[l]] = 1;
                        }
                    } else continue;
                } else continue;
            } else continue;
        }
    }
  
	return 1;
}

void fs_debug()
{
	union fs_block block;

	disk_read(0, block.data);
    if(block.super.magic==FS_MAGIC){
        printf("magic number is valid\n");
        printf("superblock:\n");
	    printf("    %d blocks\n",block.super.nblocks);
	    printf("    %d inode blocks\n",block.super.ninodeblocks);
	    printf("    %d inodes\n",block.super.ninodes);

        for (size_t i = 1; i <= block.super.ninodeblocks; i++)
        {
            disk_read(i, block.data);

            for (size_t j = 0; j < INODES_PER_BLOCK; j++)
            {
                if (block.inode[j].isvalid==1)
                {
                    printf("inode: %ld\n", j+((i-1)*INODES_PER_BLOCK)+1);
                    printf("    size: %d bytes\n", block.inode[j].size);
                    if (block.inode[j].size>0)
                    {
                        printf("    direct blocks: ");
                        for (size_t k = 0; k < POINTERS_PER_INODE; k++)
                        {
                            if (block.inode[j].direct[k]==0) break;                            
                            printf("%d ", block.inode[j].direct[k]);
                        }
                        if (block.inode[j].indirect!=0)
                        {
                            printf("\n    indirect block: %d\n", block.inode[j].indirect);
                            printf("    indirect data blocks: ");

                            union fs_block pointers_block;
                            disk_read(block.inode[j].indirect, pointers_block.data);

                            for (size_t l = 0; l < POINTERS_PER_BLOCK; l++)
                            {
                                if (pointers_block.pointers[l]==0) break;                           
                                printf("%d ", pointers_block.pointers[l]);
                            }
                        } 
                        printf("\n");
                    } else continue;

                } else continue;
                
            }
            
        }
    } else {
        printf("magic number is unvalid, make 'format'!\n");
    }

}

int fs_getsize(int inumber)
{
    union fs_block block;

    int nblock = divide_up(inumber, INODES_PER_BLOCK);
    int index_of_inode = get_inode_index(inumber);

    disk_read(nblock, block.data);

    if (block.inode[index_of_inode].isvalid==0) return -1;
    else return block.inode[inumber].size;
}

int fs_create()
{
    union fs_block block;

    int ninodeblocks = get_n_inode_blocks();

    for (size_t i = 1; i <= ninodeblocks; i++)
    {
        disk_read(i, block.data);

        for (size_t j = 0; j < INODES_PER_BLOCK; j++)
        {
            if (block.inode[j].isvalid == 0)
            {
                block.inode[j].isvalid = 1;
                block.inode[j].size = 0;
                block.inode[j].indirect = 0;
                disk_write(i, block.data);
                return j+((i-1)*INODES_PER_BLOCK)+1;
            } else continue;
        }
    }
	return -1;
}

int fs_delete(int inumber)
{
    union fs_block block;

    int nblock = divide_up(inumber, INODES_PER_BLOCK);
    int index_of_inode = get_inode_index(inumber);

    disk_read(nblock, block.data);

    if (block.inode[index_of_inode].isvalid == 0)
    {
        printf("The file does not exist\n");
    } else {
        block.inode[index_of_inode].isvalid = 0;
        
    }
    
    return 1;
}

int fs_read(int inumber, char *data, int length, int offset)
{
	return 0;
}

int fs_write(int inumber, const char *data, int length, int offset)
{
	return 0;
}

int divide_up(int numerator, int denominator){
    return numerator/denominator + ((numerator%denominator == 0) ? 0 : 1);
}

int get_n_inode_blocks(){
    return divide_up(disk_size(), 10);
}

int number_of_data_block(){
    int number_of_blocks = disk_size();
    int ninodeblocks = get_n_inode_blocks();
    int data_blocks = number_of_blocks-1-ninodeblocks;
    
    return data_blocks;
}

int get_inode_index(int inumber){
    int nblock = divide_up(inumber, INODES_PER_BLOCK);
    int inode_index = inumber-(INODES_PER_BLOCK*(nblock-1))-1;

    return inode_index;
}