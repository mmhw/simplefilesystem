#include "shell.h"
#include "fs.h"
#include "disk.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main(int argc, char *argv[])
{
    
    char *diskName = argv[1];
    int nblocks = atoi(argv[2]);

    char line[1024];
    char cmd[1024];
    char arg1[1024];
    char arg2[1024];
    int args;

    if(argc != 3)
    {
        printf("use: %s <diskname> <nblocks>\n",argv[0]);
        return 1;
    }
    
    if(!disk_init(diskName, nblocks))
    {
        printf("couldn't initialize %s: %s\n", diskName, strerror(errno));
        return 1;
    }
    
    printf("opened emulated disk image %s with %d blocks\n", diskName, disk_size());
    
    while (1)
    {
        printf(" Simple F.S.> ");
        fflush(stdout);

        if(!fgets(line,sizeof(line),stdin)) break;      // Takes the stdin and puts it in tha "line"

        if(line[0]=='\n') continue;
        line[strlen(line)-1] = 0;       // delete the enter character

        args = sscanf(line,"%s %s %s",cmd,arg1,arg2);

        if(args==0) continue;

        if(!strcmp(cmd,"format")) format(args);
        else if(!strcmp(cmd,"mount")) mount(args);
        else if(!strcmp(cmd,"debug")) debug(args);
        else if(!strcmp(cmd,"getsize")) getsize(args, arg1);
        else if(!strcmp(cmd,"create")) create(args);
        else if(!strcmp(cmd,"delete")) delete(args, arg1);
        else if(!strcmp(cmd,"cat")) cat(args, arg1);
        else if(!strcmp(cmd,"copyin")) copyin(args, arg1, arg2);
        else if(!strcmp(cmd,"copyout")) copyout(args, arg1, arg2);
        else if(!strcmp(cmd,"help")) help();
        else if(!strcmp(cmd,"quit")) break;
        else if(!strcmp(cmd,"exit")) break;
        else {
            printf("unknown command: %s\n",cmd);
            printf("type 'help' for a list of commands.\n");
            //result = 1;
        }
    }      
    printf("closing emulated disk.\n");
	disk_close();

    return 0; 
}

void format(int args)
{
    if(args==1) {
       if(fs_format()) {
           printf("disk formatted.\n");
       } else {
           printf("format failed!\n");
       }
    } else {
        printf("use: format\n");
    }
}

void mount(int args)
{
    if(args==1) {
       if(fs_mount()){
           printf("disk mounted.\n");
       } else {
           printf("mount failed!\n");
       }
    } else {
        printf("use: mount\n");
    }
}

void debug(int args)
{
    if(args==1) {
       fs_debug(); 
    } else {
        printf("use: debug\n");
    }
}

void getsize(int args, char *arg1)
{
    if(args==2) {
        int inumber = atoi(arg1);
		int result = fs_getsize(inumber);
        if(result>=0) {
            printf("inode %d has size %d\n",inumber,result);
        } else {
            printf("getsize failed!\n");
        }
    } else {
        printf("use: getsize <inumber>\n");
    }
}

void create(int args)
{
    if(args==1) {
        int inumber = fs_create();
        if(inumber>=0) {
            printf("created inode %d\n",inumber);
        } else {
            printf("create failed!\n");
        }
    } else {
        printf("use: create\n");
    }
}

void delete(int args, char *arg1)
{
    if(args==2) {
        int inumber = atoi(arg1);
        if(fs_delete(inumber)) {
            printf("inode %d deleted.\n",inumber);
        } else {
            printf("delete failed!\n");
        }
    } else {
        printf("use: delete <inumber>\n");
    }
}

void cat(int args, char *arg1)
{
    if(args==2) {
        int inumber = atoi(arg1);
        if(!do_copyout(inumber,"/dev/stdout")) {
            printf("cat failed!\n");
        }
    } else {
        printf("use: cat <inumber>\n");
    }
}

void copyin(int args, char *arg1, char *arg2)
{
    if(args==3) {
        int inumber = atoi(arg2);
        if(do_copyin(arg1,inumber)) {
            printf("copied file %s to inode %d\n",arg1,inumber);
        } else {
            printf("copy failed!\n");
        }
    } else {
        printf("use: copyin <filename> <inumber>\n");
    }
}

void copyout(int args, char *arg1, char *arg2)
{
    if(args==3) {
        int inumber = atoi(arg1);
        if(do_copyout(inumber,arg2)) {
            printf("copied inode %d to file %s\n",inumber,arg2);
        } else {
            printf("copy failed!\n");
        }
    } else {
        printf("use: copyout <inumber> <filename>\n");
    }
}

void help()
{
    printf("Commands are:\n");
    printf("    format\n");
    printf("    mount\n");
    printf("    debug\n");
    printf("    getsize <inode>\n");
    printf("    create\n");
    printf("    delete  <inode>\n");
    printf("    cat     <inode>\n");
    printf("    copyin  <file> <inode>\n");
    printf("    copyout <inode> <file>\n");
    printf("    help\n");
    printf("    quit\n");
    printf("    exit\n");    
}

static int do_copyin( const char *filename, int inumber )
{
	FILE *file;
	int offset=0, result, actual;
	char buffer[16384];

	file = fopen(filename,"r");
	if(!file) {
		printf("couldn't open %s: %s\n",filename,strerror(errno));
		return 0;
	}

	while(1) {
		result = fread(buffer,1,sizeof(buffer),file);
		if(result<=0) break;
		if(result>0) {
			actual = fs_write(inumber,buffer,result,offset);
			if(actual<0) {
				printf("ERROR: fs_write return invalid result %d\n",actual);
				break;
			}
			offset += actual;
			if(actual!=result) {
				printf("WARNING: fs_write only wrote %d bytes, not %d bytes\n",actual,result);
				break;
			}
		}
	}

	printf("%d bytes copied\n",offset);

	fclose(file);
	return 1;
}

static int do_copyout( int inumber, const char *filename )
{
	FILE *file;
	int offset=0, result;
	char buffer[16384];

	file = fopen(filename,"w");
	if(!file) {
		printf("couldn't open %s: %s\n",filename,strerror(errno));
		return 0;
	}

	while(1) {
		result = fs_read(inumber,buffer,sizeof(buffer),offset);
		if(result<=0) break;
		fwrite(buffer,1,result,file);
		offset += result;
	}

	printf("%d bytes copied\n",offset);

	fclose(file);
	return 1;
}