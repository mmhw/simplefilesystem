void format(int args);
void mount(int args);
void debug(int args);
void getsize(int args, char *arg1);
void create(int args);
void delete(int args, char *arg1);
void cat(int args, char *arg1);
void copyin(int args, char *arg1, char *arg2);
void copyout(int args, char *arg1, char *arg2);
void help();

static int do_copyin( const char *filename, int inumber );
static int do_copyout( int inumber, const char *filename );